
var gulp = require('gulp'),
    gUtil = require('gulp-util'),
    webpack = require('webpack'),
    webPackConfig = require('./webpack.config.js');

gulp.task('watch', function() {
  gulp.watch(['src/*.js'], ['bundle']);
});

gulp.task('bundle', function(cb) {
  webpack(webPackConfig, function(err, stats) {
    if(err) {
      console.error(err.message);
      throw new gUtil.PluginError('webpack', err);
    }

    gUtil.log("[webpack]", stats.toString({
      colors: gUtil.colors.supportsColor,
      hash: true,
      version: true,
      timings: true,
      chunks: true,
      chunkModules: true,
      cached: true,
      cachedAssets: true
    }));
    cb();
  });
});

gulp.task('default', ['bundle', 'watch']);
