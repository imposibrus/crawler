
var DEBUG = true;

module.exports = {
  entry: {
    main: './src/start.js',
    worker: './src/worker.js'
  },
  target: 'node',
  externals: /^[a-z][a-z\.\-0-9]*$/,

  output: {
    path: './build',
    publicPath: './',
    filename: '[name].js',
    libraryTarget: 'commonjs2'
  },

  bail: true,
  profile: true,

  cache: DEBUG,
  debug: DEBUG,

  stats: {
    colors: true,
    reasons: DEBUG
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.json/,
        loader: 'json-loader'
      }
    ]
  }
};
