
var Redis = require('ioredis'),
    Promise = require('bluebird'),
    md5 = require('md5'),
    debug = require('debug'),
    redis = new Redis(),
    debugLog = debug('queue');

function addToQueue(task) {
  var id = md5(task.url.href);
  return redis.exists('test:queue:tasks:' + id).then(function(exist) {
    if(exist) {
      debugLog('task', id, 'already exist in queue');
      return Promise.resolve();
    }

    return redis.multi()
        .mset('test:queue:tasks:' + id, JSON.stringify(task))
        .rpush('test:queue:pending', id)
        .exec()
        .then(function() {
          debugLog('task', id, 'added to queue');
        });
  });
}


module.exports = addToQueue;
