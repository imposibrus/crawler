
var fs = require('fs'),
    path = require('path'),
    http = require('http'),
    url = require('url'),
    EventEmitter = require('events'),
    Redis = require('ioredis'),
    Promise = require('bluebird'),
    _ = require('lodash'),
    jsdom = require('jsdom'),
    md5 = require('md5'),
    debug = require('debug'),
    jQueryScript = fs.readFileSync('./node_modules/jquery/dist/jquery.js'),
    packageJSON = require('../package.json'),
    addToQueue = require('./addToQueue'),
    Url = require('./Url');

const imagesExtensions = ['.png', '.jpg', '.bmp', '.jpeg', '.gif'];

const debugLog = debug('worker');

class Worker extends EventEmitter {
  constructor() {
    super();

    this.redis = new Redis();

    this.processedCount = 0;

    this.needStop = false;

    this.JSDOM = new Promise((resolve, reject) => {
      jsdom.env({
        html: '<html><body></body></html>',
        src: [jQueryScript],
        done: (err, window) => {
          if(err) {
            return reject(err);
          }

          this.window = window;
          resolve(window);
        }
      });
    });
  }

  fail(taskId, err) {
    debugLog(this.options.name, 'failed', taskId, 'with error:');
    debugLog(err.stack);
    // TODO: add error information to task
    return this.redis.multi()
        .lrem('test:queue:processing', 1, taskId)
        .rpush('test:queue:failed', taskId)
        .exec()
        .then(() => {
          if(this.needStop) {
            return this.emit('stop');
          } else {
            return this.start();
          }
        });
  }

  complete(taskId) {
    debugLog(this.options.name, 'processed', taskId);
    return this.redis.multi()
        .lrem('test:queue:processing', 1, taskId)
        .rpush('test:queue:completed', taskId)
        .exec()
        .then(() => {
          debugLog(this.options.name, 'completed', taskId);
          process.send({type: 'processedCount', count: this.processedCount++});
          if(this.needStop) {
            return this.emit('stop');
          } else {
            return this.start();
          }
        });
  }

  process(taskId, task) {
    debugLog(this.options.name, 'start processing', taskId, 'with payload', task);
    return this.getHTML(task).then((html) => {
      if(html === false) {
        debugLog(task.url.href + ' loaded, but it is image. do not adding to DOM');
        return this.complete(taskId);
      }

      debugLog(task.url.href + ' loaded. adding to DOM');
      return this.JSDOM.then(window => {
        window.document.write(html);
        return this.parseLinks(task.url.href).then((linksForProcessing) => {
          //debugLog('on page found links:', linksForProcessing);
          linksForProcessing.map(addToQueue);
          return this.complete(taskId);
        });
      });
    });
  }

  setupWorker(options) {
    this.options = options;
    this.startedUrlParsed = url.parse(this.options.startUrl);
    this.excludedExtensions = !this.options.images ? imagesExtensions : [];
  }

  start() {
    return this.redis.brpoplpush('test:queue:pending', 'test:queue:processing', 10).then((taskId) => {
      if(_.isNull(taskId)) {
        this.stop(() => {
          process.exit(0);
        });
        this.emit('stop');
        return Promise.resolve();
      }
      return this.redis.get('test:queue:tasks:' + taskId).then((task) => {
        return this.process(taskId, JSON.parse(task))
            .catch((err) => {
              return this.fail(taskId, err);
            });
      });
    });
  }

  getHTML(task) {
    return new Promise((resolve, reject) => {
      let getOptions = {
        hostname: task.url.hostname,
        port: task.url.port || 80,
        path: task.url.path,
        headers: {
          'Referer': task.refer,
          'User-Agent': 'Crawler v' + packageJSON.version
        },
        keepAlive: true
      };

      http.get(getOptions, res => {
        if(res.statusCode >= 400 || (!this.options.program.redirects && (res.statusCode == 301 || res.statusCode == 302))) {
          return reject(new Error(res.statusCode));
        }
        if(imagesExtensions.indexOf(path.extname(task.url.path).toLowerCase()) != -1) {
          return resolve(false);
        }
        var resData = '';
        res.on('data', data => {
          resData += data.toString();
        });
        res.on('end', () => {
          resolve(resData);
        });
      }).on('error', err => {
        debugLog('error on requesting', task);
        reject(err);
      });
    });
  }

  parseLinks(refer) {
    var $ = this.window.$,
        linksArr = $('a[href]').map((i, aTag) => $(aTag).attr('href')).get();

    if(this.options.images) {
      linksArr = linksArr.concat($('img[src]').map((i, img) => $(img).attr('src')).get());
    }

    var parsedLinks = Worker.deDupUrls(Worker.removeHashFromUrls(this.filterLocalLinks(this.makeAbsolute(Worker.parseUrls(linksArr))))),
        linksForProcessing = parsedLinks
            .filter(link => this.excludedExtensions.indexOf(path.extname(link.href).toLowerCase()) == -1 );

    return Promise.all(this.filterExitsTasks(linksForProcessing)).then((links) => {
      linksForProcessing = _.compact(links).map(link => new Url(link, refer));

      return linksForProcessing;
    });
  }

  filterExitsTasks(links) {
    return links.map((link) => {
      return new Promise((resolve) => {
        this.redis.exists('test:queue:tasks:' + md5(link.href)).then(function(exist) {
          if(exist) {
            resolve(false);
          } else {
            resolve(link);
          }
        });
      });
    });
  }

  static parseUrls(links) {
    return links.map(link => url.parse(link));
  }

  makeAbsolute(hrefUrls) {
    return hrefUrls.map(parsedUrl => {
      if(!parsedUrl.hostname) {
        let newUrl = this.startedUrlParsed.protocol + '//' + this.startedUrlParsed.host + parsedUrl.path;
        return url.parse(newUrl);
      }
      return parsedUrl;
    });
  }

  static removeHashFromUrls(hrefUrls) {
    return hrefUrls.map(hrefUrl => {
      hrefUrl.hash = null;
      hrefUrl.href = hrefUrl.href.split('#')[0];
      return hrefUrl;
    });
  }

  static deDupUrls(hrefUrls) {
    return _(hrefUrls).unique().value();
  }

  filterLocalLinks(hrefUrls) {
    return hrefUrls.filter(parsedUrl => {
      return parsedUrl.host == this.startedUrlParsed.host && parsedUrl.path != ';';
    });
  }

  stop(cb) {
    this.needStop = true;
    this.on('stop', () => {
      this.redis.quit(cb);
    });
  }
}


var worker = new Worker();

process.send({type: 'online'});

process.on('message', function(data) {
  if(data.action == 'start') {
    worker.setupWorker({
      name: 'worker' + process.pid,
      startUrl: data.link,
      images: data.program.images,
      program: data.program
    });
    worker.start();
  } else if(data.action == 'shutdown') {
    worker.stop(() => {
      process.exit(0);
    });
  }
});

