
var path = require('path');

const imagesExtensions = ['.png', '.jpg', '.bmp', '.jpeg', '.gif'];

class Url {
  constructor(linkUrl, refer) {
    this.url = linkUrl;
    this.type = imagesExtensions.indexOf(path.extname(linkUrl.href).toLowerCase()) != -1 ? 'image' : 'link';
    this.refer = refer;
  }
}

module.exports = Url;
