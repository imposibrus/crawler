
var program = require('commander'),
    master = require('./master'),
    packageJSON = require('../package.json');

var _parseInt = (str) => parseInt(str, 10);

program
    .version(packageJSON.version)
    .usage('[options] url')
    .option('-n, --limit [number]', 'Processing links limit', _parseInt, 1000000)
    .option('-c, --concurrency [number]', 'Concurrency', _parseInt, 2)
    .option('-o, --out [type]', 'Save urls in file as', /^(tank|images)$/i, 'tank')
    .option('-i, --images', 'Check images', true)
    .option('-f, --flush', 'Flush all tasks before', false)
    .option('-r, --redirects', 'Consider that redirects is success link', false);

program
    .action(function(link) {
      console.log('Start crawling "%s"', link);

      master(link, program);
    });

program
    .parse(process.argv);

if(!program.args.length) {
  program.help();
}
