
var fs = require('fs'),
    Promise = require('bluebird'),
    _ = require('lodash');

class Logger {
  constructor(redis, program, timeout, hostname) {
    this.redis = redis;
    this.program = program;
    this.timeout = timeout;
    this.loadedLogFileWrote = 0;
    this.errorsLogFileWrote = 0;
    this.stat = {
      loaded: 0,
      error: 0,
      queue: 0
    };

    if(program.out == 'tank') {
      fs.writeFileSync('./ammo.txt', ['[Connection: close]', '[Host: '+ hostname +']'].join('\n') + '\n');
    } else if(program.out == 'images') {
      fs.writeFileSync('./images.log', '');
    }

    this.foreverPromise = this.forever(this.writeLog.bind(this), this.timeout);
  }

  forever(fn, timeout = 500) {
    return fn().cancellable().delay(timeout).then(() => {
      return this.forever(fn, timeout);
    });
  }

  stop() {
    return this.foreverPromise.cancel().catch(Promise.CancellationError, () => {
      return this.writeLog();
    });
  }

  writeLog() {
    var promise;
    //console.log('start writing log file with:', this.loadedLogFileWrote, this.errorsLogFileWrote);
    if(this.program.out == 'tank') {
      promise = this.writeTankOutFile(this.loadedLogFileWrote, this.errorsLogFileWrote);
    } else if(this.program.out == 'images') {
      promise = this.writeImagesOutFile(this.loadedLogFileWrote, this.errorsLogFileWrote);
    }
    return promise.then((data) => {
      return this.getKeysLength().then((results) => {
        let [loaded, error, queue] = [results[0][1], results[1][1], results[2][1]];
        let [loadedDiff, errorDiff, queueDiff] = [
          loaded - this.stat.loaded > 0 ? '+' : '',
          error - this.stat.error > 0 ? '+' : '',
          queue - this.stat.queue > 0 ? '+' : ''
        ];

        console.log('loaded', loaded, 'links (' + loadedDiff + (loaded - this.stat.loaded) + ')');
        console.log('error on load in', error, 'links (' + errorDiff + (error - this.stat.error) + ')');
        console.log('in queue', queue, 'links (' + queueDiff + (queue - this.stat.queue) + ')');
        console.log('');
        this.stat.loaded = loaded;
        this.stat.error = error;
        this.stat.queue = queue;

        this.loadedLogFileWrote += data.completed;
        this.errorsLogFileWrote += data.failed;
        //console.log('write logs resolved with:', this.loadedLogFileWrote, this.errorsLogFileWrote);
      });
    });
  }

  getKeysLength() {
    return this.redis.multi()
        .llen('test:queue:completed')
        .llen('test:queue:failed')
        .llen('test:queue:pending')
        .exec();
  }

  _getTasksByType(type, start = 0, stop = -1) {
    //console.log('_getTasksByType', type, start, stop);
    return this.redis.lrange('test:queue:' + type, start, stop).then((tasksIds) => {
      return Promise.all(tasksIds.map((taskId) => {
        return this.redis.get('test:queue:tasks:' + taskId).then(JSON.parse);
      }));
    });
  }

  getCompletedTasks(start = 0, stop = -1) {
    return this._getTasksByType('completed', start, stop);
  }

  getFailedTasks(start = 0, stop = -1) {
    return this._getTasksByType('failed', start, stop);
  }

  getTasks(cb) {
    return Promise.join(this.getCompletedTasks(this.loadedLogFileWrote), this.getFailedTasks(this.errorsLogFileWrote), cb);
  }

  writeTankOutFile() {
    return this.getTasks((completed, failed) => {
      let data = _.difference(
          completed
              .filter(link => link.type == 'link' )
              .map(link => link.url.href),
          failed
              .filter(link => link.type == 'link' )
              .map(link => link.url.href)
      );

      if(data.length) {
        fs.writeFileSync('./ammo.txt', data.join('\n') + '\n', {flag: 'a'});
      }
      return Promise.resolve({completed: completed.length, failed: failed.length});
    });
  }

  writeImagesOutFile() {
    //console.log('this.loadedLogFileWrote', this.loadedLogFileWrote);
    //console.log('this.errorsLogFileWrote', this.errorsLogFileWrote);
    return this.getTasks((completed, failed) => {
      let completedList = completed
              .filter(link => link.type == 'image' )
              .map(link => 'success ' + link.url.href + ' "'+ link.refer +'"'),
          failedList = failed
              .filter(link => link.type == 'image' )
              .map(link => 'error ' + link.url.href + ' "'+ link.refer +'"'),
          data = [].concat(completedList, failedList);

      if(data.length) {
        //console.log('logs data', data);
        fs.writeFileSync('./images.log', data.join('\n') + '\n', {flag: 'a'});
      }
      return Promise.resolve({completed: completed.length, failed: failed.length});
    });
  }
}

module.exports = Logger;
