
var url = require('url'),
    child_process = require('child_process'),
    Redis = require('ioredis'),
    _ = require('lodash'),
    addToQueue = require('./addToQueue'),
    Url = require('./Url'),
    Logger = require('./Logger'),
    redis = new Redis();

var checkFlush = (program) => {
  if(program.flush) {
    return redis.multi()
        .del('test:queue:completed')
        .del('test:queue:failed')
        .del('test:queue:pending')
        .del('test:queue:processing')
        .exec().then(function() {
          return redis.keys('test:queue:tasks:*').then(function(keys) {
            return Promise.all(keys.map((key) => {
              return redis.del(key);
            }));
          });
        });
  } else {
    return Promise.resolve();
  }
};

module.exports = function(link, program) {
  checkFlush(program).then(function() {
    addToQueue(new Url(url.parse(link), ''));

    var logger = new Logger(redis, program, 3000, url.parse(link).hostname);

    var processedCounts = {},
        workers = {};

    for(let i = 0; i < program.concurrency; i++) {
      let worker = child_process.fork('./build/worker.js');
      worker.on('message', function(data) {
        if(data.type == 'online') {
          workers[worker.pid] = worker;
          worker.send({action: 'start', link, program: _.pick(program, ['limit', 'concurrency', 'out', 'images'])});
        } else if(data.type == 'processedCount') {
          processedCounts[worker.pid] = data.count;
          if(_.sum(processedCounts) >= program.limit) {
            _.each(workers, function(worker) {
              worker.send({action: 'shutdown'});
            });
          }
        }
      }).on('exit', function(code, signal) {
        delete workers[worker.pid];
        console.log('worker', worker.pid, 'died with code', code, 'and signal', signal);
        if(_.size(workers) == 0) {
          logger.stop().then(function() {
            redis.quit(function() {
              console.log('all links processed');
              process.exit(0);
            });
          });
        }
      });
    }
  });
};


